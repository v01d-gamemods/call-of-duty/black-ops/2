import dearpygui.dearpygui as dpg

from trainerbase.gui import (
    add_codeinjection_to_gui,
    simple_trainerbase_menu,
)
from injections import *


@simple_trainerbase_menu("Call of Duty®: Black Ops II", 600, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Code Injections", tag="code_injections"):
            add_codeinjection_to_gui(infinite_hp, "Infinite HP", "F1")
            add_codeinjection_to_gui(infinite_ammo, "Infinite Ammo", "F2")
            add_codeinjection_to_gui(rapid_fire, "Rapid Fire", "F3")
