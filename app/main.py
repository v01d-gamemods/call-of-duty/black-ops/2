import os

from trainerbase.scriptengine import system_script_engine
from gui import run_menu


def start_script_engines():
    system_script_engine.start()


def main():
    run_menu(on_initialized=start_script_engines)
    os._exit(0)  # pylint: disable=protected-access


if __name__ == "__main__":
    main()
