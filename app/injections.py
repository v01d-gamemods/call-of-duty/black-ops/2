from trainerbase.codeinjection import CodeInjection, AllocatingCodeInjection
from trainerbase.memory import pm


infinite_hp = CodeInjection(
    pm.base_address + 0x3B5049,
    b"\x90" * 6,  # 6 nops
)

rapid_fire = CodeInjection(
    pm.base_address + 0x32A1DA,
    b"\x90" * 2,
)

infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x167F3E,
    """
        pop ebx
        dec edx
        and edx, ecx
        mov dword [eax], 999
    """,
    original_code_length=6,
)
